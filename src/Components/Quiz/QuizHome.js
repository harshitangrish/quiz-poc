import React, { useState } from "react";
import "./QuizHome.css";

import Navbar from "../Navbar/Navbar";
import Question from "../Question/Question";
const questionsArr = [
    {
        "questionNo": 1,
        "questionText": ["In a certain store, the profit is 320% of the cost. If the cost increases by 25% but the selling price remains constant, approximately what percentage of the selling price is the profit?"],
        "userSubmittedAnswer": "",
        "isCorrect": false,
        "answerOptions": [
            { "answerText": "30%", "isCorrect": false },
            { "answerText": "70%", "isCorrect": true },
            { "answerText": "90%", "isCorrect": false },
            { "answerText": "200%", "isCorrect": false }
        ]
    },
    {
        "questionNo": 2,
        "questionText": ["Fate smiles ________ those who untiringly grapple with stark realities of life."],
        "userSubmittedAnswer": "",
        "isCorrect": false,
        "answerOptions": [
            { "answerText": "with", "isCorrect": false },
            { "answerText": "over", "isCorrect": false },
            { "answerText": "on", "isCorrect": true },
            { "answerText": "round", "isCorrect": false }
        ]
    },
    {
        "questionNo": 3,
        "questionText": ["The cost price of 20 articles is the same as the selling price of x articles. If the profit is 25%, then the value of x is:"],
        "userSubmittedAnswer": "",
        "isCorrect": false,
        "answerOptions": [
            { "answerText": "15", "isCorrect": false },
            { "answerText": "16", "isCorrect": true },
            { "answerText": "18", "isCorrect": false },
            { "answerText": "20", "isCorrect": false }
        ]
    },
    {
        "questionNo": 4,
        "questionText": ["The miser gazed _______ at the pile of gold coins in front of him."],
        "userSubmittedAnswer": "",
        "isCorrect": false,
        "answerOptions": [
            { "answerText": "avidly", "isCorrect": true },
            { "answerText": "admiringly", "isCorrect": false },
            { "answerText": "thoughtfully", "isCorrect": false },
            { "answerText": "earnestly", "isCorrect": false }
        ]
    },
    {
        "questionNo": 5,
        "questionText": [`I felt the wall of the tunnel shiver. The master alarm squealed through my earphones. Almost simultaneously, Jack yelled down to me that there was a warning light on. Fleeting but spectacular sights snapped into ans out of view, the snow, the shower of debris, the moon, looming close and big, the dazzling sunshine for once unfiltered by layers of air. The last twelve hours before re-entry were particular bone-chilling. During this period, I had to go up in to command module. Even after the fiery re-entry splashing down in 81o water in south pacific, we could still see our frosty breath inside the command module.`
            , "The word 'Command Module' used twice in the given passage indicates perhaps that it deals with"
        ],
        "userSubmittedAnswer": "",
        "isCorrect": false,
        "answerOptions": [
            { "answerText": "an alarming journey", "isCorrect": false },
            { "answerText": "a commanding situation", "isCorrect": false },
            { "answerText": "a journey into outer space", "isCorrect": true },
            { "answerText": "a frightful battle.", "isCorrect": false }
        ]
    },
    {
        "questionNo": 6,
        "questionText": [`I felt the wall of the tunnel shiver. The master alarm squealed through my earphones. Almost simultaneously, Jack yelled down to me that there was a warning light on. Fleeting but spectacular sights snapped into ans out of view, the snow, the shower of debris, the moon, looming close and big, the dazzling sunshine for once unfiltered by layers of air. The last twelve hours before re-entry were particular bone-chilling. During this period, I had to go up in to command module. Even after the fiery re-entry splashing down in 81o water in south pacific, we could still see our frosty breath inside the command module.`
            , "Which one of the following reasons would one consider as more as possible for the warning lights to be on?"
        ],
        "userSubmittedAnswer": "",
        "isCorrect": false,
        "answerOptions": [
            { "answerText": "There was a shower of debris.", "isCorrect": false },
            { "answerText": "Jack was yelling.", "isCorrect": false },
            { "answerText": "A catastrophe was imminent.", "isCorrect": true },
            { "answerText": "The moon was looming close and big.", "isCorrect": false }
        ]
    },
    {
        "questionNo": 7,
        "questionText": [
            `I felt the wall of the tunnel shiver. The master alarm squealed through my earphones. Almost simultaneously, Jack yelled down to me that there was a warning light on. Fleeting but spectacular sights snapped into ans out of view, the snow, the shower of debris, the moon, looming close and big, the dazzling sunshine for once unfiltered by layers of air. The last twelve hours before re-entry were particular bone-chilling. During this period, I had to go up in to command module. Even after the fiery re-entry splashing down in 81o water in south pacific, we could still see our frosty breath inside the command module.`
            , "The statement that the dazzling sunshine was 'for once unfiltered by layers of air' means"
        ],
        "userSubmittedAnswer": "",
        "isCorrect": false,
        "answerOptions": [
            { "answerText": "that the sun was very hot", "isCorrect": false },
            { "answerText": "that there was no strong wind", "isCorrect": false },
            { "answerText": "that the air was unpolluted", "isCorrect": false },
            { "answerText": "none of above", "isCorrect": true }
        ]
    },
    {
        "questionNo": 8,
        "questionText": ["ReactJs covers only the view layer of the application"],
        "userSubmittedAnswer": "",
        "isCorrect": false,
        "answerOptions": [
            { "answerText": "True", "isCorrect": true },
            { "answerText": "False", "isCorrect": false }
        ]
    },
    {
        "questionNo": 9,
        "questionText": [
            `But I did not want to shoot the elephant. I watched him beating his bunch of grass against his knees, with the preoccupied grandmotherly air that elephants have. It seemed to me that it would be murder to shoot him. I had never shot an elephant and never wanted to. (Somehow it always seems worse to kill large animal.) Besides, there was the beast's owner to be considered. But I had got to act quickly. I turned to some experienced-looking Burmans who had been there when we arrived, and asked them how the elephants had been behaving. They all said the same thing; he took no notice of you if you left him alone, but he might charge if you went too close to him.`
            , "The phrase 'Preoccupied grandmotherly air' signifies"
        ],
        "userSubmittedAnswer": "",
        "isCorrect": false,
        "answerOptions": [
            { "answerText": "being totally unconcerned", "isCorrect": false },
            { "answerText": "pretending to be very busy", "isCorrect": false },
            { "answerText": "a very superior attitude", "isCorrect": false },
            { "answerText": "calm, dignified and affectionate disposition", "isCorrect": true }
        ]
    },
    {
        "questionNo": 10,
        "questionText": [
            `But I did not want to shoot the elephant. I watched him beating his bunch of grass against his knees, with the preoccupied grandmotherly air that elephants have. It seemed to me that it would be murder to shoot him. I had never shot an elephant and never wanted to. (Somehow it always seems worse to kill large animal.) Besides, there was the beast's owner to be considered. But I had got to act quickly. I turned to some experienced-looking Burmans who had been there when we arrived, and asked them how the elephants had been behaving. They all said the same thing; he took no notice of you if you left him alone, but he might charge if you went too close to him.`
            , "From the passage it appears that the author was"
        ],
        "userSubmittedAnswer": "",
        "isCorrect": false,
        "answerOptions": [
            { "answerText": "an inexperienced hunter", "isCorrect": false },
            { "answerText": "kind and considerate", "isCorrect": true },
            { "answerText": "possessed with fear", "isCorrect": false },
            { "answerText": "a worried man", "isCorrect": false }
        ]
    },
    {
        "questionNo": 11,
        "questionText": [
            `But I did not want to shoot the elephant. I watched him beating his bunch of grass against his knees, with the preoccupied grandmotherly air that elephants have. It seemed to me that it would be murder to shoot him. I had never shot an elephant and never wanted to. (Somehow it always seems worse to kill large animal.) Besides, there was the beast's owner to be considered. But I had got to act quickly. I turned to some experienced-looking Burmans who had been there when we arrived, and asked them how the elephants had been behaving. They all said the same thing; he took no notice of you if you left him alone, but he might charge if you went too close to him.`
            , "The author did not want to shoot the elephant because he"
        ],
        "userSubmittedAnswer": "",
        "isCorrect": false,
        "answerOptions": [
            { "answerText": "was afraid of it", "isCorrect": false },
            { "answerText": "did not have the experience of shooting big animals", "isCorrect": true },
            { "answerText": "did not wish to kill animal which was not doing anybody any harm", "isCorrect": false },
            { "answerText": "did not find the elephant to be ferocious", "isCorrect": false }
        ]
    },
    {
        "questionNo": 12,
        "questionText": ["ReactJs uses virtual DOM to improve performance"],
        "userSubmittedAnswer": "",
        "isCorrect": false,
        "answerOptions": [
            { "answerText": "True", "isCorrect": true },
            { "answerText": "False", "isCorrect": false }
        ]
    },
];

function QuizHome(props) {
    const [currentQuestion, setCurrentQuestion] = useState(0);
    const [questions, setQuestions] = useState(questionsArr);
    const totalQuestions = questions.length;

    const handleUserResponse = (selectedValue, isCorrect) => {
        questionsArr[currentQuestion].userSubmittedAnswer = selectedValue;
        questionsArr[currentQuestion].isCorrect = isCorrect;
        setQuestions(questionsArr);
    };

    const goToPreviousQuestion = () => {
        if (currentQuestion !== 0) {
            setCurrentQuestion((currentQuestion - 1));
        }
    };

    const goToNextQuestion = () => {
        if (currentQuestion < (totalQuestions - 1)) {
            setCurrentQuestion((currentQuestion + 1));
        }
        else{
            props.handleSubmit(questions,totalQuestions);
        }
    };

    const handleQuizEnd = ()=>{
        props.handleSubmit(questions,totalQuestions);
    }

    return (
        <React.Fragment>
            <Navbar handleTimer={handleQuizEnd} />
            <div className="container-fluid">
                <div className="container-quiz" >
                    <Question currentQuestionContent={questions[currentQuestion]}
                        handleResponse={handleUserResponse}
                        totalQuestions={totalQuestions}
                        handleFinishButton={handleQuizEnd}
                    />
                </div>
                <div className="row navigation-container w-100">
                    <div className="col-md-12 bg-light py-3">
                        <div className="container-quiz d-flex justify-content-center align-items-center">
                            <button type="button"
                                onClick={goToPreviousQuestion}
                                className={currentQuestion === 0 ? "btn btn-primary mr-md-3 disabled" : "btn btn-primary mr-md-3"}>
                                Previous
                            </button>
                            <span>{currentQuestion + 1}/{totalQuestions}</span>
                            {
                                currentQuestion < (totalQuestions - 1) ? <button type="button" onClick={goToNextQuestion} className="btn btn-primary ml-md-3">Next</button> : <button type="button" onClick={goToNextQuestion} className="btn btn-primary ml-md-3">Submit</button>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}
export default QuizHome;
