import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import TermsAndConditions from './Pages/TermsAndConditions/TermsAndConditions';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter , Route , Switch} from 'react-router-dom';
import AnswerAndQuiz from './Pages/AnswerAndQuiz/AnswerAndQuiz';

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
        <Switch>
            <Route exact path = "/" component={TermsAndConditions}/>
            
            <Route path = "/quiz" component={ AnswerAndQuiz }/>
            
            <Route path = "*" component={TermsAndConditions}/>
        </Switch>
    </BrowserRouter>
    
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
