import React, { useState } from "react";
import Answer from "../../Components/Answer/Answer";
import QuizHome from "../../Components/Quiz/QuizHome";
import "./AnswerAndQuiz.css";

function AnswerAndQuiz(props) {
    const [questions, setQuestions] = useState([]);
    const [totalQuestions, setTotalQuestions] = useState(0);
    const [quiz, setQuiz] = useState(true);


    const handleSubmit = (questions, totalQuestions) => {
        setQuestions(questions);
        setTotalQuestions(totalQuestions);
        setQuiz(false);
    };

    return (
        <React.Fragment>
            {quiz===true?<QuizHome history={props.history} handleSubmit={handleSubmit} />:<Answer questions={questions} totalQuestions={totalQuestions} />}    
        </React.Fragment>

    );
}

export default AnswerAndQuiz;