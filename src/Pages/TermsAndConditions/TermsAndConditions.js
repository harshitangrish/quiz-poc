import React from "react";
import "./TermsAndConditions.css";
import Alert from '@material-ui/lab/Alert';


function TermsAndCondtions(props) {
    const handleNavigation = ()=>{
        props.history.push('/quiz');
    }
    return (

        <React.Fragment>
            <nav className="navbar navbar-expand-sm navbar-light bg-light">
                 {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a className="navbar-brand flex-grow-1" href="#">Dummy Quiz</a>
            </nav>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12 mt-4 text-center">
                        <h1 className="title">Instructions</h1>
                    </div>
                    <div className="mt-3 mb-4 col-md-8 offset-md-2">
                        <Alert severity="warning">Please read all the instructions carefully before begining with quiz.</Alert>
                        <ul className="mt-4">
                            <li>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sollicitudin, felis sed ultrices pharetra, felis orci laoreet tellus, vel ultricies.
                                </li>
                            <li>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam viverra tempus enim, ut aliquam dui egestas vitae. Mauris vitae lectus rutrum, dapibus dui et, sagittis ex. Donec at dapibus lorem.
                                </li>
                            <li>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sollicitudin, felis sed ultrices pharetra, felis orci laoreet tellus, vel ultricies.
                                </li>
                            <li>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam viverra tempus enim, ut aliquam dui egestas vitae. Mauris vitae lectus rutrum, dapibus dui et, sagittis ex. Donec at dapibus lorem.
                                </li>
                            <li>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sollicitudin, felis sed ultrices pharetra, felis orci laoreet tellus, vel ultricies.
                                </li>
                            <li>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam viverra tempus enim, ut aliquam dui egestas vitae. Mauris vitae lectus rutrum, dapibus dui et, sagittis ex. Donec at dapibus lorem.
                                </li>
                            <li>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sollicitudin, felis sed ultrices pharetra, felis orci laoreet tellus, vel ultricies.
                                </li>
                            <li>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam viverra tempus enim, ut aliquam dui egestas vitae. Mauris vitae lectus rutrum, dapibus dui et, sagittis ex. Donec at dapibus lorem.
                                </li>
                            <li>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sollicitudin, felis sed ultrices pharetra, felis orci laoreet tellus, vel ultricies.
                                </li>
                            <li>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam viverra tempus enim, ut aliquam dui egestas vitae. Mauris vitae lectus rutrum, dapibus dui et, sagittis ex. Donec at dapibus lorem.
                                </li>
                        </ul>
                        <div className="text-center">
                        <button type="button" onClick={handleNavigation} className= "btn btn-primary">Begin quiz</button>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12 bg-light py-3">
                        <div className="container-quiz d-flex justify-content-center align-items-center">
                        <small>Copyright &copy;: 2016-2021 DummyQuiz</small>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

export default TermsAndCondtions;