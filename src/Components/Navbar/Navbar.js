import React, { useEffect, useState } from "react";

import './Navbar.css';

function Navbar(props) {

  const [time, setTime] = useState({});
  const [seconds, setSeconds] = useState(600);
  const [timer,setTimer] = useState(0);

  const secondsToTime = (secs) => {
    let hours = Math.floor(secs / (60 * 60));

    let divisor_for_minutes = secs % (60 * 60);
    let minutes = Math.floor(divisor_for_minutes / 60);

    let divisor_for_seconds = divisor_for_minutes % 60;
    let second = Math.ceil(divisor_for_seconds);

    let obj = {
      "h": hours,
      "m": minutes,
      "s": second
    };
    return obj;
  }
  const startTimer = () => {
    if (timer === 0 && seconds > 0) {
      setTimer(setInterval(countDown, 1000));
    }
  }

  const countDown = () => {
    // Remove one second, set state so a re-render happens.
    setSeconds(prevSeconds => prevSeconds - 1)
    
  }

  useEffect(() => {
    let timeLeftVar = secondsToTime(seconds);
    setTime(timeLeftVar);
    startTimer();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  
  useEffect(() => {
    let updatedTimeObj = secondsToTime(seconds);
    setTime(updatedTimeObj);
    if (seconds === 0) {
      setTimer(clearInterval(timer));
      props.handleTimer();
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [seconds]);



  return (
    <nav className="navbar navbar-expand-sm navbar-light bg-light">
      {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
      <a className="navbar-brand flex-grow-1" href="#">Dummy Quiz</a>
      <span className="navbar-text">
      {time.m}min:{time.s}s 
    </span>
    </nav>

  );
}

export default Navbar;
