import React, { useState, useEffect } from "react";
import "./Question.css";



function Question(props) {

    const { currentQuestionContent, handleResponse, totalQuestions } = props;
    const [questionContent, setQuestionContent] = useState(currentQuestionContent);

    const handleAnswer = (event, answerOption) => {
        handleResponse(event.target.id,answerOption.isCorrect)
    };

    const handleFinishButton = ()=>{
        props.handleFinishButton();
    }

    useEffect(() => {
        setQuestionContent(props.currentQuestionContent)
    }, [props.currentQuestionContent]);

    return (
        <React.Fragment>
            <div className="row">
                <div className="col-md-12 pt-2 d-flex justify-content-between align-items-center">
                    <div className="text-center">Question {questionContent.questionNo}/{totalQuestions}</div>
                    <button type="button" className="finish-button btn btn-danger" onClick={handleFinishButton} >Finish Test</button>
                </div>
            </div>
            <div className="row mt-5">
                <div className="col-md-12">
                    <div className="question">
                        {
                            questionContent.questionText.map((question, index) => {
                                if (index === (questionContent.questionText.length - 1)) {
                                    return <p key={index}>{"Question " + questionContent.questionNo + ": " + question}</p>
                                }
                                else {
                                    return <p key={index}>{question}</p>
                                }

                            })
                        }
                    </div>
                    <div className="Answers">
                        <form>
                            {
                                questionContent.answerOptions.map((answerOption, index) => {
                                    return (
                                        <div className="form-check" key={index}>
                                            {questionContent.userSubmittedAnswer === answerOption.answerText ? <input className="form-check-input" type="radio" name={questionContent.questionNo} id={answerOption.answerText} value={answerOption.answerText} checked={true} onChange={(e) => { handleAnswer(e,answerOption) }} /> : <input className="form-check-input" type="radio" name={questionContent.questionNo} id={answerOption.answerText} value={answerOption.answerText} onChange={(e) => { handleAnswer(e, answerOption) }} />}
                                            <label className="form-check-label" htmlFor={answerOption.answerText}>
                                                {answerOption.answerText}
                                            </label>
                                        </div>
                                    );
                                })
                            }
                        </form>
                    </div>
                </div>
            </div>
        </React.Fragment>

    );
}

export default Question;