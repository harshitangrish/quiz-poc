import React from "react";

import "./Answer.css";

function Answer(props) {
    const { questions, totalQuestions } = props;

    const attemptedQuestions = (questions.filter((question)=>{
            return question.userSubmittedAnswer !== ""
    })).length

    const displayOptions = (questionContent, answerOption, index) => {
        
        if (questionContent.userSubmittedAnswer === answerOption.answerText && answerOption.isCorrect === true) {
            return (
                <div className="form-check" key={index}>
                    <input className="form-check-input" type="radio" name={questionContent.questionNo} id={answerOption.answerText} value={answerOption.answerText} checked={true} disabled={true} />
                    <label className="form-check-label text-success" htmlFor={answerOption.answerText}>
                        {answerOption.answerText}
                    </label>
                </div>
            );
        }
        else if (questionContent.userSubmittedAnswer === answerOption.answerText && answerOption.isCorrect === false) {
            return (
                <div className="form-check" key={index}>
                    <input className="form-check-input" type="radio" name={questionContent.questionNo} id={answerOption.answerText} value={answerOption.answerText} checked={true} disabled={true} />
                    <label className="form-check-label text-danger" htmlFor={answerOption.answerText}>
                        {answerOption.answerText}
                    </label>
                </div>
            );
        }
        else if (questionContent.userSubmittedAnswer !== answerOption.answerText && answerOption.isCorrect === true) {
            return (
                <div className="form-check" key={index}>
                    <input className="form-check-input" type="radio" name={questionContent.questionNo} id={answerOption.answerText} value={answerOption.answerText} disabled={true} />
                    <label className="form-check-label text-success" htmlFor={answerOption.answerText}>
                        {answerOption.answerText}
                    </label>
                </div>
            );

        }
        else {
            return (
                <div className="form-check" key={index}>
                    <input className="form-check-input" type="radio" name={questionContent.questionNo} id={answerOption.answerText} value={answerOption.answerText} disabled={true} />
                    <label className="form-check-label" htmlFor={answerOption.answerText}>
                        {answerOption.answerText}
                    </label>
                </div>
            );
        }
    };


    return (
        <React.Fragment>
            <nav className="navbar navbar-expand-sm navbar-light bg-light">
                 {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a className="navbar-brand flex-grow-1" href="#">Dummy Quiz</a>
            </nav>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12 mt-4 text-center">
                        <h1 className="title">Answer Details</h1>
                    </div>
                    <div className="mt-3 mb-4 col-md-8 offset-md-2">
                        <div>Total Attempted Questions: {attemptedQuestions}/{totalQuestions}</div>

                        {questions.map((questionContent, indexQuestions) => {
                            return (
                                <React.Fragment >
                                    <div className="mt-4" key={indexQuestions}>
                                        {questionContent.questionText.map((question, indexQuestion) => {
                                            return (
                                                <React.Fragment>
                                                    {indexQuestion === (questionContent.questionText.length - 1) ? <p key={indexQuestion}>{"Question " + questionContent.questionNo + ": " + question}</p> : <p key={indexQuestion}>{question}</p>}
                                                    {indexQuestion === (questionContent.questionText.length - 1) ?questionContent.answerOptions.map((answerOption, index) => {
                                                        return (
                                                            displayOptions(questionContent, answerOption, index)
                                                        );
                                                    }):null
                                                    }
                                                </React.Fragment>
                                            );
                                        })
                                        }
                                    </div>
                                </React.Fragment>



                            );
                        })}


                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12 bg-light py-3">
                        <div className="container-quiz d-flex justify-content-center align-items-center">
                            <small>Copyright &copy;: 2016-2021 DummyQuiz</small>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>

    );
}

export default Answer;